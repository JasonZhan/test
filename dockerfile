# # 使用官方的 alpine 映像檔作為基礎
# FROM alpine

# # 複製您的程式檔案到容器中
# COPY TEST.txt /app/TEST.txt

# # 定義容器運行時的工作目錄
# WORKDIR /app

# # 指定容器啟動時執行的命令
# CMD ["cat", "TEST.txt"]


# 使用基礎的 Alpine 映像
FROM alpine:latest

# 設置工作目錄
WORKDIR /app

# 複製當前目錄下的所有內容到容器的工作目錄
COPY . .

# 暴露應用運行的端口 (可選)
EXPOSE 8080

# 指定容器啟動時執行的命令 (在這裡沒有特定命令)
CMD ["sh"]